import 'dart:io';
import 'dart:math';

void main(List<String> arguments) {
  Island map = Island(10, 10);
  Person jasper = Person(0, 0, map);
  map.addFish(map, 8);
  map.addTrash(map, 4);
  map.setPerson(jasper);
  while (!map.isFinished) {
    map.showMap();
    String direction = stdin.readLineSync()!;
    if (direction == 'q' || direction == 'Q') {
      print("Exit game.");
      print("Bye bye");
      break;
    }
    jasper.walk(direction.toUpperCase());
  }
}

class Object {
  int x;
  int y;
  String symbol;

  Object(this.x, this.y, this.symbol);

  bool isOn(int x, int y) {
    return this.x == x && this.y == y;
  }
}

class Fish extends Object {
  int x;
  int y;
  String symbol = "F";

  Fish(this.x, this.y) : super(x, y, "F");
}

class Trash extends Object {
  int x;
  int y;
  String symbol = "T";

  Trash(this.x, this.y) : super(x, y, "T");
}

class Island {
  int width;
  int height;
  late Person jasper;
  bool isFinished = false;
  List<Object> objects = [];
  int objCount = 0;

  Island(this.width, this.height);

  void setPerson(Person jasper) {
    this.jasper = jasper;
    addObj(jasper);
  }

  void addObj(Object obj) {
    objects.add(obj);
    objCount++;
  }

  void printSymbolOn(int x, int y) {
    String symbol = '-';
    for (int o = 0; o < objCount; o++) {
      if (objects[o].isOn(x, y)) {
        symbol = objects[o].symbol;
      }
    }
    stdout.write(symbol);
  }

  void showMap() {
    showTitle();
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        printSymbolOn(x, y);
      }
      showNewLine();
    }
  }

  void showTitle() {
    print("Island");
  }

  void showNewLine() {
    print("");
  }

  void showPerson() {
    print(jasper.symbol);
  }

  bool inMap(int x, int y) {
    return (x >= 0 && x < width) && (y >= 0 && y < height);
  }

  bool isFish(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Fish && objects[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }

  bool isTrash(int x, int y) {
    for (int o = 0; o < objCount; o++) {
      if (objects[o] is Trash && objects[o].isOn(x, y)) {
        return true;
      }
    }
    return false;
  }

  void addFish(Island map, int maxFish) {
    for (int i = 0; i <= maxFish; i++) {
      Fish fish = Fish(
          Random().nextInt(map.width - 1), Random().nextInt(map.height - 1));
      if (map.objects.contains(fish)) {
        i--;
      } else {
        map.addObj(fish);
      }
    }
  }

  void addTrash(Island map, int maxTrash) {
    for (int i = 0; i <= maxTrash; i++) {
      Trash trash = Trash(
          Random().nextInt(map.width - 1), Random().nextInt(map.height - 1));
      if (map.objects.contains(trash)) {
        i--;
      } else {
        map.addObj(trash);
      }
    }
  }
}

class Person extends Object {
  Island map;
  late int x;
  late int y;
  int count = 0;
  String symbol = "J";

  Person(this.x, this.y, this.map) : super(x, y, "J");

  /*
    walkN() = north
    walkS() = south
    walkE() = east
    walkW() = west
  */

  bool walk(String direction) {
    switch (direction) {
      case 'W':
        if (walkN()) return false;
        break;
      case 'S':
        if (walkS()) return false;
        break;
      case 'D':
        if (walkE()) return false;
        break;
      case 'A':
        if (walkW()) return false;
        break;
      default:
        return false;
    }

    checkTrash();
    checkFish();
    return true;
  }

  void checkTrash() {
    if (map.isTrash(x, y)) {
      print("It is not a fish.");
      count = count - 10;
      print("Your point = $count .");
      if (count == 0) {
        map.showMap();
        print("Game over.");
        map.isFinished = true;
      }
    }
  }

  void checkFish() {
    if (map.isFish(x, y)) {
      print("You found a fish.");
      count = count + 10;
      print("Your point = $count .");
      if (count == 50) {
        map.showMap();
        print("You win.");
        print("Your point = $count .");
        map.isFinished = true;
      }
    }
  }

  bool canWalk(int x, int y) {
    return map.inMap(x, y);
  }

  bool walkW() {
    if (canWalk(x - 1, y)) {
      x = x - 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkE() {
    if (canWalk(x + 1, y)) {
      x = x + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkS() {
    if (canWalk(x, y + 1)) {
      y = y + 1;
    } else {
      return true;
    }
    return false;
  }

  bool walkN() {
    if (canWalk(x, y - 1)) {
      y = y - 1;
    } else {
      return true;
    }
    return false;
  }
}
